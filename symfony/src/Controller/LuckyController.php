<?php
// src/Controller/LuckyController.php
namespace App\Controller; //déclaration espace de nom (espace au début du fichier)[avant tout autre code except declare]

use Symfony\Component\HttpFoundation\Response;//revoi une reponse http

class LuckyController
{
    public function number(): Response
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
    }
}